CREATE TABLE Docentes (
	cod_doc varchar(10),
	nomb_doc varchar(30),
	telefono varchar(10),
	email varchar(50)
);
ALTER TABLE Docentes ADD CONSTRAINT dk_docentes PRIMARY KEY(cod_doc);

CREATE TABLE Cursos (
	cod_cur varchar(6),
	nomb_cur varchar(30),
	cod_doc varchar(10)
);
ALTER TABLE Cursos ADD CONSTRAINT rk_cursos PRIMARY KEY(cod_cur);
CREATE TABLE Estudiantes (
	cod_est varchar(10),
	nomb_est varchar(30),
	telefono varchar(10),
	email varchar(50),
	cod_programa varchar(10)
);
ALTER TABLE Estudiantes ADD CONSTRAINT ek_estudiantes PRIMARY KEY(cod_est);
CREATE TABLE Cohortes (
	cod_est varchar(10),
	cod_cur varchar(10),
	nota1 float4,
	nota2 float4,
	nota3 float4
);

CREATE TABLE Programas (
	cod_programa varchar(6),
	nomb_programa varchar(40)
);
ALTER TABLE Programas ADD CONSTRAINT gk_programas PRIMARY KEY(cod_programa);

CREATE TABLE Administrador (
	contraseña varchar(10),
	nomb_admin varchar(30),
	telefono varchar(10),
	email varchar(50)
);

CREATE TABLE docente_actual (
	cod varchar(10)
);
ALTER TABLE docente_actual ADD CONSTRAINT pk_docente_actual PRIMARY KEY(cod);

CREATE TABLE estudiante_actual (
	cod varchar(10)
);

CREATE TABLE curso_actual (
    cod_cursoactual VARCHAR (10)
); 
ALTER TABLE curso_actual ADD CONSTRAINT ak_curso_actual PRIMARY KEY (cod_cursoactual);

ALTER TABLE estudiante_actual ADD CONSTRAINT pk_estudiante_actual PRIMARY KEY(cod);

ALTER TABLE Cohortes ADD CONSTRAINT POSEE FOREIGN KEY (cod_est) REFERENCES Estudiantes(cod_est) MATCH FULL ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE Cohortes ADD CONSTRAINT TIENE FOREIGN KEY (cod_cur) REFERENCES Cursos(cod_cur) MATCH FULL ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cursos ADD CONSTRAINT ENSEÑA FOREIGN KEY (cod_doc) REFERENCES docentes(cod_doc) MATCH FULL ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE Estudiantes ADD CONSTRAINT PERTENECE FOREIGN KEY (cod_programa) REFERENCES Programas(cod_programa) MATCH FULL ON DELETE CASCADE ON UPDATE CASCADE;


CREATE OR REPLACE FUNCTION insertar_docente(cod_doc varchar,nomb_doc varchar,telefono varchar,email varchar) RETURNS varchar AS $$
DECLARE
BEGIN
INSERT INTO docentes(cod_doc,nomb_doc,telefono,email) VALUES (cod_doc,nomb_doc,telefono,email);
return 'OK';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insertar_cursos(cod_cur varchar,nomb_cur varchar,cod_doc varchar) RETURNS  varchar AS $$
DECLARE
BEGIN
INSERT INTO cursos(cod_cur,nomb_cur,cod_doc) VALUES (cod_cur,nomb_cur,cod_doc);
return 'OK';
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION insertar_estudiante(cod_est varchar,nomb_est varchar,telefono varchar,email varchar, cod_programa varchar) RETURNS varchar AS $$
DECLARE
BEGIN
INSERT INTO estudiantes(cod_est,nomb_est,telefono,email,cod_programa) VALUES (cod_est,nomb_est,telefono,email,cod_programa);
return 'OK';
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION val_docente() RETURNS trigger AS $$
BEGIN
    IF NEW.cod_doc IS NULL THEN
        RAISE EXCEPTION 'El codigo del docente es obligatorio';
    END IF;
    IF NEW.nomb_doc IS NULL THEN
        RAISE EXCEPTION 'El nombre del docente es obligatorio';
    END IF;
    IF NEW.telefono IS NULL THEN
        RAISE EXCEPTION 'El telefono es obligatorio';
    END IF;
    IF NEW.email IS NULL THEN
        RAISE EXCEPTION 'El correo es obligatorio';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER val_docente BEFORE INSERT OR UPDATE ON docentes
FOR EACH ROW EXECUTE PROCEDURE val_docente();

CREATE OR REPLACE FUNCTION val_curso() RETURNS trigger AS $$
BEGIN
    IF NEW.cod_cur IS NULL THEN
        RAISE EXCEPTION 'El codigo del curso es obligatorio';
    END IF;
    IF NEW.nomb_cur IS NULL THEN
        RAISE EXCEPTION 'El nombre del curso es obligatorio';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER val_curso BEFORE INSERT OR UPDATE ON cursos
FOR EACH ROW EXECUTE PROCEDURE val_curso();

CREATE OR REPLACE FUNCTION val_estudiante() RETURNS trigger AS $$
BEGIN
    IF NEW.cod_est IS NULL THEN
        RAISE EXCEPTION 'El codigo del estudiante es obligatorio';
    END IF;
    IF NEW.nomb_est IS NULL THEN
        RAISE EXCEPTION 'El nombre del estudiante es obligatorio';
    END IF;
    IF NEW.telefono IS NULL THEN
        RAISE EXCEPTION 'El telefono no contiene numeros negativos';
    END IF;
    IF NEW.email IS NULL THEN
        RAISE EXCEPTION 'El correo es obligatorio';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER val_estudiante BEFORE INSERT OR UPDATE ON estudiantes
FOR EACH ROW EXECUTE PROCEDURE val_estudiante();

CREATE OR REPLACE FUNCTION val_cohorte() RETURNS trigger AS $$
BEGIN
    IF NEW.cod_est IS NULL THEN
        RAISE EXCEPTION 'El codigo del estudiante es obligatorio';
    END IF;
    IF NEW.cod_cur IS NULL THEN
        RAISE EXCEPTION 'El codigo del curso es obligatorio';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER val_cohorte BEFORE INSERT OR UPDATE ON cohortes
FOR EACH ROW EXECUTE PROCEDURE val_cohorte();

CREATE OR REPLACE FUNCTION val_programa() RETURNS trigger AS $$
BEGIN
    IF NEW.cod_programa IS NULL THEN
        RAISE EXCEPTION 'El codigo del programa es obligatorio';
    END IF;
    IF NEW.nomb_programa IS NULL THEN
        RAISE EXCEPTION 'El nombre del programa es obligatorio';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER val_programa BEFORE INSERT OR UPDATE ON programas
FOR EACH ROW EXECUTE PROCEDURE val_programa();

CREATE OR REPLACE FUNCTION val_admin() RETURNS trigger AS $$
BEGIN
    IF NEW.contraseña IS NULL THEN
        RAISE EXCEPTION 'La contraseña es obligatoria';
    END IF;
    IF NEW.nomb_admin IS NULL THEN
        RAISE EXCEPTION 'El codigo del curso es obligatorio';
    END IF;
    IF NEW.telefono IS NULL THEN
        RAISE EXCEPTION 'El telefono es obligatorio';
    END IF;
    IF NEW.email IS NULL THEN
        RAISE EXCEPTION 'El correo es obligatorio';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER val_admin BEFORE INSERT OR UPDATE ON administrador
FOR EACH ROW EXECUTE PROCEDURE val_admin();

CREATE OR REPLACE VIEW view_nota_final AS SELECT *FROM cohortes;
CREATE OR REPLACE VIEW view_carreras AS SELECT *FROM programas;
